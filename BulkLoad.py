import pyodbc 
from itertools import islice
from datetime import datetime
import pandas as pd

# Get start time.
start = datetime.now()

# Read text file to data frame.
df = pd.read_csv ( "./Tilraun 2021-04-19.csv", delimiter = ",", header=1)
df.columns = [ "Id", "Category", "Name", "Price", "RentalName", "Ranking", "DateOfCapture", "StartDate"]

# Connect to database and prepare query.
query = "insert PythonBulk ( Id, Category, Name, Price, RentalName, Ranking, DateOfCapture, StartDate ) values ( ?, ?, ?, ?, ?, ?, ?, ? )"
conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=Uxmal;'
                      'Database=Steini;'
                      'Trusted_Connection=yes;')
cursor = conn.cursor()

# Truncate table.
cursor.execute ( "truncate table PythonBulk")

# Insert dataframe data into database.
for index, row in df.iterrows():
  cursor.execute ( query, row.Id, row.Category, row.Name, row.Price, row.RentalName, row.Ranking, row.DateOfCapture, row.StartDate )

# Cleanup
conn.commit()
cursor.close()
end = datetime.now()
duration = end - start

# Result.
print ( f"{index} row(s) processed" )
print ( f"Execution duration: {duration}" )
