use Steini;

drop table if exists PythonBulk
create table PythonBulk
(
  Id            int               not null, 
  Category      varchar ( 100 )       null,
  Name          varchar ( 100 )       null,
  Price         decimal ( 18, 1 )     null,
  RentalName    varchar ( 100 )       null,
  Ranking       decimal ( 5, 1 )      null,
  DateOfCapture date                  null,
  StartDate     date                  null,
)
go
